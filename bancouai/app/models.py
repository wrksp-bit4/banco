from django.db import models

# Create your models here.
class Oferta(models.Model):
    rut = models.CharField(max_length=8)
    monto = models.IntegerField()
    fechaRegistro = models.CharField(max_length=20)
    nombreEjecutivo = models.CharField(max_length=10)
    estadoOferta = models.CharField(max_length=10)
