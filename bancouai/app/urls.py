from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('acceso', views.home),
    path('inicio', views.home),
    path('adminCreditos', views.adminCreditos),
    path('ingresaCredito', views.ingresaCredito),
    path('volverAdminCreditos', views.volverAdminCreditos),
    path('listaRut', views.listaRut),
    path('inicioConsulta', views.inicioConsulta),
    path('consultarCredito', views.consultarCredito),
]
