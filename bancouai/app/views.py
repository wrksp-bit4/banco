from django.http import HttpResponse
from django.shortcuts import render
from datetime import datetime
from .models import Oferta
import requests as http
import json

def index(request):
    return render(request, 'app/index.html')

def home(request):
    return render(request, 'app/login.html')

def inicio(request):
    return render(request, 'app/login.html')

def adminCreditos(request):
    usuario = request.POST['usuario']
    ofertas = Oferta.objects.all()
    context = {
        'usuario': usuario,
        'ofertas': ofertas,
    }
    return render(request, 'app/adminCreditos.html', context)

def ingresaCredito(request):
    urlAPI = "https://qbmbuvbhbxbjmjo.form.io/operaciones/submission"
    rutCliente = request.POST['rutCliente']
    dvRutCliente = request.POST['dvRutCliente']
    #montoCredito = request.POST['montoCredito']
    nombreEjecutivo = request.POST['ejecutivo']
    rutCompleto = rutCliente+'-'+dvRutCliente
    estadoOferta = "VIGENTE"
    #Consulta API
    req = http.get(urlAPI)
    #Leemos el Json
    data = json.loads(req.text)

    #Ordenamos el Json para que sea legible
    #dataj = json.dumps(data,indent=4)
    #print(dataj)
    existe = False
    for key in data:
        rutRespuesta = key['data']['textField']
        montoRespuesta = key['data']['montoDisponible']
        print(rutRespuesta)
        print(montoRespuesta)
        if rutRespuesta == rutCompleto:
            existe = True
            print("Rut existe")
            break

    #Validamos si RUT fue encontrado
    if existe:
        montoCredito = montoRespuesta
    else:
        montoCredito = 0
        estadoOferta = "NO VIGENTE"
    
    #Fecha actual
    now = datetime.now()
    fechaFormateada = now.strftime("%d-%m-%Y %H:%M:%S")

    Oferta.objects.create(rut=rutCompleto,monto=montoCredito,fechaRegistro=fechaFormateada,nombreEjecutivo=nombreEjecutivo,estadoOferta=estadoOferta)

    ofertas = Oferta.objects.all()
    context = {
        'rutCompleto': rutCompleto,
        'ofertas': ofertas,
        'nombreEjecutivo': nombreEjecutivo,
    }
    return render(request, 'app/listaRut.html', context)


def ingresaCreditoOLD(request):
    rutCliente = request.POST['rutCliente']
    dvRutCliente = request.POST['dvRutCliente']
    montoCredito = request.POST['montoCredito']
    nombreEjecutivo = request.POST['ejecutivo']
    rutCompleto = rutCliente+'-'+dvRutCliente

    #Fecha actual
    now = datetime.now()
    fechaFormateada = now.strftime("%d-%m-%Y %H:%M:%S")

    Oferta.objects.create(rut=rutCompleto,monto=montoCredito,fechaRegistro=fechaFormateada,nombreEjecutivo=nombreEjecutivo,estadoOferta="ACTIVA")

    ofertas = Oferta.objects.all()
    context = {
        'rutCompleto': rutCompleto,
        'ofertas': ofertas,
        'nombreEjecutivo': nombreEjecutivo,
    }
    return render(request, 'app/listaRut.html', context)

def volverAdminCreditos(request):
    usuario = request.POST['ejecutivo']
    ofertas = Oferta.objects.all()
    context = {
        'usuario': usuario,
        'ofertas': ofertas,
    }
    return render(request, 'app/adminCreditos.html', context)

def listaRut(request):
    nombreEjecutivo = request.POST['ejecutivo']
    #Eliminamos registtros
    entries= Oferta.objects.all()
    entries.delete()

    ofertas = Oferta.objects.all()
    context = {
        'rutCompleto': 'N/A',
        'ofertas': ofertas,
        'nombreEjecutivo': nombreEjecutivo,
    }
    return render(request, 'app/listaRut.html', context)

def inicioConsulta(request):
    return render(request, 'app/consultaCredito.html')

def consultarCredito(request):
    rutCliente = request.POST['rutCliente']
    dvRutCliente = request.POST['dvRutCliente']
    rutCompleto = rutCliente+'-'+dvRutCliente
    ofertas = None
    ofertas = Oferta.objects.all().filter(rut=rutCompleto)
    cantidad = len(ofertas)
    context = {
        'rutCompleto': rutCompleto,
        'ofertas': ofertas,
        'cantidad': cantidad,
    }
    return render(request, 'app/resultadoConsulta.html', context)